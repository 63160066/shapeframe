import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class RectangleFrame extends JFrame {
    public RectangleFrame(){
        super("Rectangle");
        this.setSize(600, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        JLabel lblWidth = new JLabel("width : ", JLabel.TRAILING);
        lblWidth.setSize(50, 20);
        lblWidth.setLocation(5,5);
        lblWidth.setBackground(Color.LIGHT_GRAY);
        lblWidth.setOpaque(true);
        this.add(lblWidth);

        JLabel lblLength = new JLabel("length : ", JLabel.TRAILING);
        lblLength.setSize(50, 20);
        lblLength.setLocation(5,30);
        lblLength.setBackground(Color.LIGHT_GRAY);
        lblLength.setOpaque(true);
        this.add(lblLength);

        JTextField txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(60, 5);
        this.add(txtWidth);

        JTextField txtLength = new JTextField();
        txtLength.setSize(50, 20);
        txtLength.setLocation(60, 30);
        this.add(txtLength);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 30);
        this.add(btnCalculate);

        JLabel lblResult = new JLabel("Rectangle width = ??? length = ??? Area = ??? Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(600, 50);
        lblResult.setLocation(0, 80);
        lblResult.setBackground(Color.ORANGE);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener(){ 
            @Override
            public void actionPerformed(ActionEvent e){
                try {
                    String strWidth = txtWidth.getText();
                    double width = Double.parseDouble(strWidth);  
                    String strLength = txtLength.getText();
                    double length = Double.parseDouble(strLength); 
                    Rectangle rectangle = new Rectangle(width, length);
                    lblResult.setText("Rectangle w = " + String.format("%.2f",rectangle.getWidth())
                                + " l = " + String.format("%.2f",rectangle.getLength())
                                + " area = " + String.format("%.2f",rectangle.calArea())
                                + " perimeter = " + String.format("%.2f",rectangle.calPerimeter())
                    );   
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error!! : Please input number", 
                    "Error", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtWidth.requestFocus();
                    txtLength.setText("");
                    txtLength.requestFocus();
                }
            }
        });


    }
    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }
}
