import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CircleFrame {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Circle");
        frame.setSize(600, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblRadius = new JLabel("radius : ", JLabel.TRAILING);
        lblRadius.setSize(50, 20);
        lblRadius.setLocation(5,5);
        lblRadius.setBackground(Color.LIGHT_GRAY);
        lblRadius.setOpaque(true);
        frame.add(lblRadius);

        JTextField txtRadius = new JTextField();
        txtRadius.setSize(50, 20);
        txtRadius.setLocation(60, 5);
        frame.add(txtRadius);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);

        JLabel lblResult = new JLabel("Circle radius = ??? Area = ??? Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(600, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.CYAN);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        //Event Driven
        
        btnCalculate.addActionListener(new ActionListener(){ //Anonymous class
            @Override
            public void actionPerformed(ActionEvent e){
                try {
                    //1. insert text from txtRadius to strRadius
                    String strRadius = txtRadius.getText();
                    //2. from strRadius to radius : double by parseDouble
                    double radius = Double.parseDouble(strRadius); //--NumberFormatException 
                    //3. instance object Circle(radius) "circle"
                    Circle circle = new Circle(radius);
                    //Circle circle = null;
                    //4. update lblResult with data from circle to show on screen
                    lblResult.setText("Circle r =" + String.format("%.2f",circle.getRadius())
                                + " area = " + String.format("%.2f",circle.calArea())
                                + " perimeter = " + String.format("%.2f",circle.calPerimeter())
                    );   
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error!! : Please input number", 
                    "Error", JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();
                }
                
                /*catch (NumberFormatException ex) {
                    System.out.println("Error!! : Number Format Exception");
                } catch (NullPointerException ex) {
                    System.out.println("Error!! : Null Pointer Exception");
                } catch (Exception ex) {
                    System.out.println("Error!! : Another Exception");
                    //System.out.println("Error!!" + ex.getMessage());
                    //System.out.println("Error!!" + ex.getCause());
                }
                */
            }
        });

        frame.setVisible(true);
    }
}
