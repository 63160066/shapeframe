import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class SquareFrame extends JFrame {
    JLabel lblSide, lblResult;
    JTextField txtSide;
    JButton btnCalculate;

    public SquareFrame(){
        super("Square");
        this.setSize(600, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        this.createLabelSide();
        this.createTextFieldSide();
        this.createButtonCalculate();
        this.createLabelResult();
        
        btnCalculate.addActionListener(new ActionListener(){ 
            @Override
            public void actionPerformed(ActionEvent e){
                try {
                    String strSide = txtSide.getText();
                    double side = Double.parseDouble(strSide);   
                    Square square = new Square(side);
                    lblResult.setText("Square s = " + String.format("%.2f",square.getSide())
                                + " area = " + String.format("%.2f",square.calArea())
                                + " perimeter = " + String.format("%.2f",square.calPerimeter())
                    );   
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(SquareFrame.this, "Error!! : Please input number", 
                    "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }
        });
       
    }

    public void createLabelSide(){
        lblSide = new JLabel("side : ", JLabel.TRAILING);
        lblSide.setSize(50, 20);
        lblSide.setLocation(5,5);
        lblSide.setBackground(Color.LIGHT_GRAY);
        lblSide.setOpaque(true);
        this.add(lblSide);
    }

    public void createTextFieldSide(){
        txtSide = new JTextField();
        txtSide.setSize(50, 20);
        txtSide.setLocation(60, 5);
        this.add(txtSide);
    }

    public void createButtonCalculate(){
        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);
    }

    public void createLabelResult(){
        lblResult = new JLabel("Square side = ??? Area = ??? Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(600, 50);
        lblResult.setLocation(0, 80);
        lblResult.setBackground(Color.GREEN);
        lblResult.setOpaque(true);
        this.add(lblResult);
    }

    public static void main(String[] args) {
        SquareFrame frame = new SquareFrame();
        frame.setVisible(true);
    }
}
