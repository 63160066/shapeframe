import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class TriangleFrame extends JFrame {
    JLabel lblHeight, lblBase, lblSide1, lblSide2, lblSide3, lblResult;
    JTextField txtHeight, txtBase, txtSide1, txtSide2, txtSide3;
    JButton btnCalculate;

    public TriangleFrame(){
        super("Triangle");
        this.setSize(600, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblHeight = new JLabel("height : ", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(5,5);
        lblHeight.setBackground(Color.LIGHT_GRAY);
        lblHeight.setOpaque(true);
        this.add(lblHeight);

        txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 5);
        this.add(txtHeight);

        lblBase = new JLabel("base : ", JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setLocation(5,30);
        lblBase.setBackground(Color.LIGHT_GRAY);
        lblBase.setOpaque(true);
        this.add(lblBase);

        txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 30);
        this.add(txtBase);

        lblSide1 = new JLabel("side1 : ", JLabel.TRAILING);
        lblSide1.setSize(50, 20);
        lblSide1.setLocation(5,55);
        lblSide1.setBackground(Color.LIGHT_GRAY);
        lblSide1.setOpaque(true);
        this.add(lblSide1);

        txtSide1 = new JTextField();
        txtSide1.setSize(50, 20);
        txtSide1.setLocation(60, 55);
        this.add(txtSide1);

        lblSide2 = new JLabel("side2 : ", JLabel.TRAILING);
        lblSide2.setSize(50, 20);
        lblSide2.setLocation(120,30);
        lblSide2.setBackground(Color.LIGHT_GRAY);
        lblSide2.setOpaque(true);
        this.add(lblSide2);

        txtSide2 = new JTextField();
        txtSide2.setSize(50, 20);
        txtSide2.setLocation(180, 30);
        this.add(txtSide2);

        lblSide3 = new JLabel("side3 : ", JLabel.TRAILING);
        lblSide3.setSize(50, 20);
        lblSide3.setLocation(120,55);
        lblSide3.setBackground(Color.LIGHT_GRAY);
        lblSide3.setOpaque(true);
        this.add(lblSide3);

        txtSide3 = new JTextField();
        txtSide3.setSize(50, 20);
        txtSide3.setLocation(180, 55);
        this.add(txtSide3);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Triangle height = ??? base = ??? side1 = ??? side2 = ??? side3 = ??? Area = ??? Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(600, 50);
        lblResult.setLocation(0, 80);
        lblResult.setBackground(Color.YELLOW);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener(){ 
            @Override
            public void actionPerformed(ActionEvent e){
                try {
                    String strHeight = txtHeight.getText();
                    double height = Double.parseDouble(strHeight);  
                    String strBase = txtBase.getText();
                    double base = Double.parseDouble(strBase);
                    String strSide1 = txtSide1.getText();
                    double side1 = Double.parseDouble(strSide1);
                    String strSide2 = txtSide2.getText();
                    double side2 = Double.parseDouble(strSide2);
                    String strSide3 = txtSide3.getText();
                    double side3 = Double.parseDouble(strSide3);
                    Triangle triange = new Triangle(height, base, side1, side2, side3);
                    lblResult.setText("Triangle h = " + String.format("%.2f",triange.getHeight())
                                + " b = " + String.format("%.2f",triange.getBase())
                                + " s1 = " + String.format("%.2f",triange.getSide1())
                                + " s2 = " + String.format("%.2f",triange.getSide2())
                                + " s3 = " + String.format("%.2f",triange.getSide3())
                                + " area = " + String.format("%.2f",triange.calArea())
                                + " perimeter = " + String.format("%.2f",triange.calPerimeter())
                    );   
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error!! : Please input number", 
                    "Error", JOptionPane.ERROR_MESSAGE);
                    txtHeight.setText("");
                    txtHeight.requestFocus();
                    txtBase.setText("");
                    txtBase.requestFocus();
                    txtSide1.setText("");
                    txtSide1.requestFocus();
                    txtSide2.setText("");
                    txtSide2.requestFocus();
                    txtSide3.setText("");
                    txtSide3.requestFocus();
                }
            }
        });


    }

    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }
}
