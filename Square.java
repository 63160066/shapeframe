public class Square extends Shape {
    private double side;

    public Square(double side){
        super("Square");
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public double calArea() {
        return Math.pow(side, 2);
    }

    @Override
    public double calPerimeter() {
        return 4*side;
    }
}
