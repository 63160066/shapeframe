public abstract class Shape {
    private String shapeName;

    public Shape(String shapeName){
        this.shapeName = shapeName;
    }

    public String getShapeName() {
        return shapeName;
    }

    public abstract double calArea();
    public abstract double calPerimeter();
}
