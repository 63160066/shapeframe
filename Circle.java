public class Circle extends Shape {
    private double radius;

    public Circle(double radius){
        super("Circle");
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double calArea(){
        return Math.PI*Math.pow(radius, 2);
    }

    @Override
    public double calPerimeter() {
        return 2*Math.PI*radius;
    }
}
